https://opentelemetry.io/docs/instrumentation/go/exporters/


docker run --rm --name jaeger \
  -e COLLECTOR_OTLP_ENABLED=true \
  -p 16686:16686 \
  -p 14268:14268 \
  -p 4317:4317 \
  -p 4318:4318 \
  jaegertracing/all-in-one:latest

http://localhost:16686/search
http://localhost:16686/trace/cf94aa8aa8f5fbffa217de5880572b7a
http://localhost:8080/rolldice
http://localhost:8080/metrics

http://localhost:9090/graph?g0.expr=&g0.tab=1&g0.stacked=0&g0.show_exemplars=0&g0.range_input=1h
http://localhost:9090/targets

docker run --rm \
    --name prom \
    -p 9090:9090 \
    -v /home/shinya/workspace/test/otel-example:/etc/prometheus \
    --network host \
    prom/prometheus

dice_rolls_total
process_cpu_seconds_total
